<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
/*use Illuminate\Support\Str;*/
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {

        return [
            'product_name' => $this->faker->text(25),
            'product_description' => $this->faker->text(100),
            'quantity' => $this->faker->numberBetween($min = 1, $max = 20),
            'price' => $this->faker->numberBetween($min = 1000, $max = 9000),
            'created_at' => $this->faker->dateTime($max = 'now', $timezone = null),
            'updated_at' => $this->faker->dateTime($max = 'now', $timezone = null),

        ];
    }
}
http://localhost/phpmyadmin/sql.php?server=1&db=flowerstore_db&table=orders&pos=0#