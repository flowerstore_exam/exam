<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
/*use App\Http\Requests\ProductRequest;*/

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
/*        $products = Product::all();

       dd($products);*/
       /*return Product::orderBy('created_at', 'DESC')->get();*/
       $products = Product::all();
       return $products;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

 /*       $products = $productRequest::create($productRequest->all());
        return response()->json(['message' => 'successfully created', 'data' => products]);*/

        $product = new Product;
        $product->product_name = $request->product_name;
        $product->product_description = $request->product_description;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $result=$product->save();
        if($result) {
            return ['message' =>'Product Added'];
        } else {
            return ['message' =>'Error'];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $result=$product->delete();
        if($result){
            return ['message' =>'Product Deleted'];
        } else {
            return ['message' =>'Error'];
        }
    }
    public function order()
    {

       $orders = Order::all();
       return $orders;

    }
    public function enable(Request $request, $id)
    {
        $product = Product::find($id);
        $product->status = $request->status;
        $result=$product->save();
        if($result){
            return ['message' =>'Product Enabled'];
        } else {
            return ['message' =>'Error'];
        }
    }
    public function disable(Request $request, $id)
    {
        $product = Product::find($id);
        $product->status = $request->status;
        $result=$product->save();
        if($result){
            return ['message' =>'Product Disabled'];
        } else {
            return ['message' =>'Error'];
        }
    }

}
