<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
    <div id="app">
       <navbar-component></navbar-component>
       <product-component></product-component>
    </div>
    <script src="{{ mix('/js/app.js') }}"></script>
</html>

